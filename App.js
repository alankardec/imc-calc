import React from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';

import { createStackNavigator } from 'react-navigation';
import Inicio from './telas/Inicio';
import Resultado from './telas/Resultado';

const Navigation = createStackNavigator({
  Inicio: {screen: Inicio},
  Resultado: {screen: Resultado}
})

export default Navigation;