import React from 'react';
import { StyleSheet, Text, TextInput, View, Button } from 'react-native';

export default class Inicio extends React.Component {

  constructor() {
    super();
    this.state = { nome: 'Desconhecido', peso: 0, altura: 0};
  }

  static navigationOptions = {
      title: 'Inicio',
  }

  calcular = () => {
    let peso = parseFloat(this.state.peso);
    let altura = parseFloat(this.state.altura);
    let imc = parseFloat(peso/Math.pow(altura, 2));

    
    return imc;
  }

  render() {
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
        
        <Text style={{fontSize: 20}}>Calculo IMC</Text>
        <TextInput
          style={styles.inputs}
          placeholder="Nome Completo"
          onChangeText={(nome) => this.setState({nome})}
        />
        
        <Text>Peso:</Text>
        <TextInput 
          style={styles.inputs}
          onChangeText={(peso) => this.setState({peso})} 
        />
        
        <Text>Altura:</Text>
        <TextInput
          style={styles.inputs}
          onChangeText={(altura) => this.setState({altura})} 
        />

        <Text style={{fontSize: 20}}>{this.state.imc}</Text>

        <Button style={{width: 400}}
          onPress={
              () => navigate("Resultado", { imc: this.calcular(), nome: this.state.nome }) 
          }
          title="Calcular"
          color="#2F4F4F"
          accessibilityLabel="Calcula IMC"
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputs: {
    height: 40,
    width:400
  },
});
