import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

export default class Resultado extends React.Component {

  constructor(props) {
    super(props);
  }
  static navigationOptions = {
    title: 'Resultado',
  }

  render() {
    const { navigation } = this.props;
    const nome = navigation.getParam('nome', 'Desconhecido');
    const imc = navigation.getParam('imc', 0);

    return (
      <View style={styles.container}>
        <Text style={{fontSize: 20 }}>Olá {nome}</Text>
        <Text style={{fontSize: 20 }}>O seu indice de massa corporal é:</Text>
        <Text style={{fontSize: 20 }}>{(imc*10000).toFixed(2)}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputs: {
    height: 40,
    width:400
  },
});
